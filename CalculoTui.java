import java.util.Scanner;

public class CalculoTui {
	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Fatorial de:");
		int valorCalculado = scan.nextInt();
		if (valorCalculado < 0) {
			System.out.print("N�o existe fatorial para n�meros negativos!");
			System.out.close();
		} else {
			int num = Calculo.calculoFatorial(valorCalculado);
			System.out.println("O fatorial de " + valorCalculado + " � igual a " + num);
			System.out.println(Calculo.VtdNumPrimo(valorCalculado));
		}
	}
}	