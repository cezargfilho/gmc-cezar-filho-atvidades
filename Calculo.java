public class Calculo {
	// Classe que calcula o fatorial de um n�mero com recursividade;
	public static int calculoFatorial(int num) {
		if (num == 0) {
			return 1;
		}
		return num * calculoFatorial(num - 1);
	}

	public static String VtdNumPrimo(int num) {
		if (num == 0) {
			return "O n�mero n�o � primo";
		} else {
			for (int j = 2; j < num; j++) {
				if (num % j == 0)
					return "O n�mero n�o � primo";
			}
			return "O n�mero � primo";
		}
	}
}